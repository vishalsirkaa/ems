<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Link;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/',function(){
	$links = Link::all();
	return view('laracrud')->with('links', $links);
});
  

Route::post('/links',function (Request $request){
	$link = Link::create($request->all());
	return Response::json($link);

});

//--GET TO EDIT ==//

Route::get('/links/{link_id?}',function ($link_id){
	$link = Link::find($link_id);

	return Response::json($link);
});

//--UPDATE  alink --//

Route::put('/links/{link_id?}',function (Request $request){

	$link = Link::find($link_id);
	$link->url = $request->url;
	$link->description = $request->description;
	$link->save();

	return Resonse::json($link);

});

//--DELETE A LINK --//

Route::delete('/links/{link_id}?}',function ($link_id){
 
   $link = Link::destroy($link_id);
   return Response::json($link);
});


Route::view('jquery','jquery');