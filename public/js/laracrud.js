jQuery(document).ready(function($){

   ////---- Open the modal to CREATE  a link -----.///

   jQuery('#btn-add').click(function(){

          jQuery('#btn-save').val("add");
          jQuery('#modalFormData').trigger("reset");
          jQuery('$linkEditorModal').modal('show');
   });


   ////---- Open the modal to UPDATE A link ----////

   jQuery('body').on('click' '.open-modal', function(){

   	  var link_id = $(this).val();

   	  $.get('links/' + link_id, function (data){
          
           jQuery('#link_id').val(data.id);
           jQuery('#link').val(data.url);
           jQuery('#description').val('update');
           jquery('#linkEditorModal').modal('show');
   	  })
   });


   // Clicking the save button on the open modal for both CREATE AND UPDATE---////

   $("#btn-save").click(function(e){

   	  $.ajaxSetup({
   	  	 headers: {
   	  	 	  'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
   	  	 	}

   	  	 });

   	   e.preventDefault();
   	      var formData ={
   	      	 url: jQuery('#lnik').val(),
   	      	 description: jQuery('#description').val(),
   	      };
   	      var state = jQuery('#btn-save').val();
   	      var type = "POST";
   	      var link_id = jQuery('#link_id').val();
   	      var ajaxurl = 'links';

   	      if(state = "update")
   	      {
   	      	type= "PUT";
   	      	ajaxurl = 'links/' + link_id;
   	      }

   	      $.ajax({
   	      	 type:type,
   	      	 url: ajaxurl,
   	      	 data: formData,
   	      	 dataType:'json',
   	      	 success: function (data)
   	      	 {
   	      	 	var link = '<tr id="link' +data.id+ '"> <td>' +data.id+ '</td><td>' + data.url + 
   	      	 	'</td><td>' +data.description + '</td>';
   	      	 	link += '<td><button class="btn btn-info open-modal" value="'+ data.id + '">Edit</button>&nbsp;';
   	      	 	link += '<button class="btn btn-danger delete-link" value="' +data.id+'">Delete</button></td></tr>';
   	      	 	if(state == "add"){
   	      	 		jQuery('#links-list').append(link);
   	      	 	}elseP{
   	      	 		$("#link" +link_id).replaceWith(link);
   	      	 	}
   	      	 	jQuery('#modalFormData').trigger("reset");
   	      	 	jQuery('$linkEditorModal').modal('hide');
   	      	 	},
   	      	 	error:function (data){
   	      	 		console.log('Error:',data);
   	      	 	}
   	      	 });
   	      });

          /////-------------DELETE A LINK AND REMOVE FROM THE PAGE --------////

          
   	  })
});